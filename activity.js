// Objective 1
// Single Room
db.rooms.insertOne({
    "name": "single",
    "accommodates": 2,
    "price": 1500,
    "description": "A simple room with all the basioc necessities",
    "rooms_available": 10,
    "isAvailable": false
});


// Objective 2
// Multiple Rooms
db.rooms.insertMany([
    {
        "name": "double",
        "accomodates": 3,
        "price": 2000,
        "description": "A room fit for a small family going on a vacation",
        "rooms_available": 5,
        "isAvailable": false
    },
    {
        "name": "queen",
        "accomodates": 4,
        "price": 4000,
        "description": "A room fit with a queen sized bed perfect for a simple getaway",
        "rooms_available": 15,
        "isAvailable": false
    }
]);


// Objective 3
// Find Method
db.rooms.find({"name": "double"});

// Objective 4
// Update Room
db.rooms.updateOne(

{"name": "queen"},
{$set: {"rooms_available": 0}}
);

// Objective 5
// Delete Method
db.rooms.deleteMany(
    { "rooms_available": 0}
);












